package com.esgi.ecommerce.boxoffice.choice.use_case;

public interface DeleteChoices {
    void execute();
}
