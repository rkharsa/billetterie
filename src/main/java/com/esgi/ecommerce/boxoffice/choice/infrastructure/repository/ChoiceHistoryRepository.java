package com.esgi.ecommerce.boxoffice.choice.infrastructure.repository;

import com.esgi.ecommerce.boxoffice.choice.infrastructure.dao.SqlChoiceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChoiceHistoryRepository extends JpaRepository<SqlChoiceHistory, Integer> {
    List<SqlChoiceHistory> findAllByIdUser(Integer idUser);
}
