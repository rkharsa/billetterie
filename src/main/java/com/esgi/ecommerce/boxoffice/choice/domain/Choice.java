package com.esgi.ecommerce.boxoffice.choice.domain;

import com.esgi.ecommerce.boxoffice.order.domain.Order;

public class Choice {
    private boolean refund;
    private Order order;

    public Choice(boolean refund, Order order) {
        this.refund = refund;
        this.order = order;
    }

    public Choice() {}

    public Order getOrder() {
        return order;
    }

    public boolean isRefund() {
        return refund;
    }

    public void setRefund(boolean refund) {
        this.refund = refund;
    }
}
