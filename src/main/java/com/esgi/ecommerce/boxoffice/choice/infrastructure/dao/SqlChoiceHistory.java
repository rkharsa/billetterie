package com.esgi.ecommerce.boxoffice.choice.infrastructure.dao;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "CHOICE_HISTORY")
public class SqlChoiceHistory {
    @Id
    @GeneratedValue
    private Integer idChoice;

    private Integer idUser;

    private Integer idOrder;

    private boolean refund;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public SqlChoiceHistory(int idUser, int idOrder, boolean refund) {
        this.idUser = idUser;
        this.idOrder = idOrder;
        this.refund = refund;
    }

    public SqlChoiceHistory() {}

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Integer getIdChoice() {
        return idChoice;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public boolean isRefund() {
        return refund;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }
}
