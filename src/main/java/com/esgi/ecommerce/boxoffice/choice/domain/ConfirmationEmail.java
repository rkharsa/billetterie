package com.esgi.ecommerce.boxoffice.choice.domain;

public interface ConfirmationEmail {
    void send(String email);
}
