package com.esgi.ecommerce.boxoffice.choice.use_case;

import com.esgi.ecommerce.boxoffice.choice.domain.Choice;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceDAO;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetChoicesHistoryImpl implements GetChoicesHistory {
    private final ChoiceDAO choiceDAO;

    @Autowired
    public GetChoicesHistoryImpl(ChoiceDAO choiceDAO) {
        this.choiceDAO = choiceDAO;
    }

    @Override
    public List<Choice> execute(Integer idUser) {
        List<Choice> results = choiceDAO.getUserChoicesHistory(idUser);
        if(results.size() == 0) throw new ChoiceNotFoundException();

        return results;
    }
}
