package com.esgi.ecommerce.boxoffice.choice.infrastructure.controller;

import com.esgi.ecommerce.boxoffice.choice.domain.*;
import com.esgi.ecommerce.boxoffice.choice.use_case.*;
import com.esgi.ecommerce.boxoffice.order.domain.Order;
import com.esgi.ecommerce.boxoffice.order.use_case.GetApiOrders;
import com.esgi.ecommerce.boxoffice.user.domain.User;
import com.esgi.ecommerce.boxoffice.user.use_case.GetApiUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/choice")
public class ChoiceController {

    private final UpdateChoices updateChoices;
    private final GetChoices getChoices;
    private final GetLastChoices getLastChoices;
    private final GetChoicesHistory getChoicesHistory;
    private final DeleteChoices deleteChoices;
    private final GetApiOrders getApiOrders;
    private final GetApiUser getApiUser;

    @Autowired
    public ChoiceController(UpdateChoices updateChoices,
                            GetChoices getChoices,
                            GetLastChoices getLastChoices,
                            GetChoicesHistory getChoicesHistory,
                            DeleteChoices deleteChoices,
                            GetApiOrders getApiOrders,
                            GetApiUser getApiUser){
        this.updateChoices = updateChoices;
        this.getChoices = getChoices;
        this.getLastChoices = getLastChoices;
        this.getChoicesHistory = getChoicesHistory;
        this.deleteChoices = deleteChoices;
        this.getApiOrders = getApiOrders;
        this.getApiUser = getApiUser;
    }

    @GetMapping
    public ResponseEntity getAllChoices() {
        try {
            List<Choice> choices = this.getChoices.execute();
            return ResponseEntity.status(HttpStatus.OK)
                    .body(choices);
        } catch(ChoiceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("No Choices Found");
        }

    }

    @GetMapping("/user/{idUser}")
    public ResponseEntity getUserLastChoices(@PathVariable Integer idUser) {
        try {
            List<Choice> choices = this.getLastChoices.execute(idUser);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(choices);
        } catch(ChoiceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("No Choices Found");
        }
    }

    @GetMapping("/user/history/{idUser}")
    public ResponseEntity getUserChoicesHistory(@PathVariable Integer idUser) {
        try {
            List<Choice> choices = this.getChoicesHistory.execute(idUser);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(choices);
        } catch(ChoiceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("No Choices Found");
        }
    }

    @PostMapping("/update")
    public ResponseEntity addOrUpdateChoices(@RequestBody ChoiceMakerDto choice) {
        List<Order> orders = getApiOrders.execute(choice.getIdUser());
        List<Choice> newChoices = new ArrayList<Choice>();
        User currentUser = getApiUser.execute(choice.getIdUser());

        for(ChoiceDto choiceDto : choice.getChoices()) {
            Order newOrder = orders.stream().
                    filter(p -> p.getId().equals(choiceDto.getIdOrder())).
                    findAny().orElse(null);

            newChoices.add(new Choice(choiceDto.isRefund(), newOrder));
        }

        try {
            updateChoices.execute(orders, newChoices, currentUser.getEmail());
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body("Your choices have been successfully made");
        } catch(UnselectedChoicesException e) {
            return ResponseEntity.badRequest()
                    .body("You must make a choice for all your orders");
        } catch(UnauthorizedChoiceException e) {
        return ResponseEntity.badRequest()
                .body("You can only make choices for your own orders");
        } catch(EmailSendFailureException e) {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body("Your choices have been successfully made, but failed to send the email");
        }
    }

    @DeleteMapping
    public void deleteAll(){
        this.deleteChoices.execute();
    }

}
