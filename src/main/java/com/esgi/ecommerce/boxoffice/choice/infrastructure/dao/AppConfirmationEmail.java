package com.esgi.ecommerce.boxoffice.choice.infrastructure.dao;

import com.esgi.ecommerce.boxoffice.choice.domain.ConfirmationEmail;
import org.springframework.stereotype.Service;

@Service
public class AppConfirmationEmail implements ConfirmationEmail {

    public AppConfirmationEmail() {}

    @Override
    public void send(String email) {
        System.out.println("Hello "+ email +", You have successfully updated your choices");
    }
}