package com.esgi.ecommerce.boxoffice.choice.infrastructure.repository;

import com.esgi.ecommerce.boxoffice.choice.infrastructure.dao.SqlChoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChoiceRepository extends JpaRepository<SqlChoice, Integer> {
    List<SqlChoice> findAllByIdUser(Integer idUser);

    SqlChoice findFirstByIdOrder(Integer idOrder);
}
