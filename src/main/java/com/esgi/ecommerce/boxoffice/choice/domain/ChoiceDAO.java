package com.esgi.ecommerce.boxoffice.choice.domain;

import java.util.List;

public interface ChoiceDAO {
    List<Choice> getAllUsersChoices();

    List<Choice> getUserChoicesHistory(Integer idUser);

    List<Choice> getUserLastChoice(Integer idUser);

    void deleteAll();

    void updateChoices(List<Choice> choices);
}
