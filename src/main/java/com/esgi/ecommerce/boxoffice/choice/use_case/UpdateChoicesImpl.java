package com.esgi.ecommerce.boxoffice.choice.use_case;

import com.esgi.ecommerce.boxoffice.choice.domain.Choice;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceDAO;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceMaker;
import com.esgi.ecommerce.boxoffice.choice.domain.ConfirmationEmail;
import com.esgi.ecommerce.boxoffice.order.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UpdateChoicesImpl implements UpdateChoices {

    private final ChoiceDAO choiceDAO;
    private final ConfirmationEmail emailSender;

    @Autowired
    public UpdateChoicesImpl(ChoiceDAO choiceDAO,
                             ConfirmationEmail emailSender) {
        this.choiceDAO = choiceDAO;
        this.emailSender = emailSender;
    }

    @Override
    public void execute(List<Order> orders, List<Choice> choices, String email) {
        List<Choice> newChoices = new ChoiceMaker(choices).compute(orders);
        choiceDAO.updateChoices(newChoices);
        emailSender.send(email);
    }


}
