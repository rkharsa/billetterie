package com.esgi.ecommerce.boxoffice.choice.use_case;

import com.esgi.ecommerce.boxoffice.choice.domain.Choice;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceDAO;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetChoicesImpl implements GetChoices {

    private final ChoiceDAO choiceDAO;

    public GetChoicesImpl(ChoiceDAO choiceDAO) {
        this.choiceDAO = choiceDAO;
    }

    @Override
    public List<Choice> execute() {
        List<Choice> results = choiceDAO.getAllUsersChoices();
        if(results.size() == 0) throw new ChoiceNotFoundException();

        return results;
    }
}
