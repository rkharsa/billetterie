package com.esgi.ecommerce.boxoffice.choice.use_case;

import com.esgi.ecommerce.boxoffice.choice.domain.Choice;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceDAO;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetLastChoicesImpl implements GetLastChoices {
    private final ChoiceDAO choiceDAO;

    @Autowired
    public GetLastChoicesImpl(ChoiceDAO choiceDAO) {
        this.choiceDAO = choiceDAO;
    }

    @Override
    public List<Choice> execute(Integer userId) {
        List<Choice> results = choiceDAO.getUserLastChoice(userId);
        if(results.size() == 0) throw new ChoiceNotFoundException();

        return results;
    }
}
