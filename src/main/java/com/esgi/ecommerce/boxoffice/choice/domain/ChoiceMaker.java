package com.esgi.ecommerce.boxoffice.choice.domain;

import com.esgi.ecommerce.boxoffice.order.domain.Order;

import java.util.List;

public class ChoiceMaker {
    private final List<Choice> allUserChoices;

    public ChoiceMaker(List<Choice> allUserChoices) {
        this.allUserChoices = allUserChoices;
    }

    public List<Choice> compute(List<Order> allUserOrders) {
        checkIfAllSelected(allUserOrders);

        allUserChoices.stream()
                .forEach(choice -> choice.setRefund(choice.isRefund()));

        return allUserChoices;
    }

    private void checkIfAllSelected(List<Order> orders) {
        for(Order order : orders) {
            try {
                 Choice choiceByOrder = allUserChoices.stream().
                        filter(c -> c.getOrder().getId().equals(order.getId())).
                        findAny().orElse(null);

                 if(choiceByOrder == null) {
                     throw new UnselectedChoicesException();
                 }

            } catch(NullPointerException e) {
                throw new UnauthorizedChoiceException();
            }

        }
    }

}
