package com.esgi.ecommerce.boxoffice.choice.infrastructure.controller;

public class ChoiceDto {
    private Integer idOrder;
    private boolean refund;

    public ChoiceDto(Integer idOrder, boolean refund) {
        this.idOrder = idOrder;
        this.refund = refund;
    }

    public boolean isRefund() {
        return refund;
    }

    public Integer getIdOrder() {
        return idOrder;
    }
}
