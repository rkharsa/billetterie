package com.esgi.ecommerce.boxoffice.choice.infrastructure.controller;

import java.util.List;

public class ChoiceMakerDto {
    private Integer idUser;
    private List<ChoiceDto> choices;

    public ChoiceMakerDto(Integer idUser, List<ChoiceDto> choices) {
        this.idUser = idUser;
        this.choices = choices;
    }

    public List<ChoiceDto> getChoices() {
        return choices;
    }

    public Integer getIdUser() {
        return idUser;
    }
}
