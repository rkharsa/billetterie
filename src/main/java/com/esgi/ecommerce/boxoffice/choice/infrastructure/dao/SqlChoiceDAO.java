package com.esgi.ecommerce.boxoffice.choice.infrastructure.dao;

import com.esgi.ecommerce.boxoffice.choice.domain.Choice;
import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceDAO;
import com.esgi.ecommerce.boxoffice.choice.infrastructure.repository.ChoiceHistoryRepository;
import com.esgi.ecommerce.boxoffice.choice.infrastructure.repository.ChoiceRepository;
import com.esgi.ecommerce.boxoffice.order.domain.Order;
import com.esgi.ecommerce.boxoffice.order.use_case.GetApiOrders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SqlChoiceDAO implements ChoiceDAO {

    private final ChoiceRepository choiceRepository;
    private final ChoiceHistoryRepository choiceHistoryRepository;
    private final GetApiOrders getApiOrders;

    @Autowired
    public SqlChoiceDAO(ChoiceRepository choiceRepository, ChoiceHistoryRepository choiceHistoryRepository, GetApiOrders getApiOrders) {
        this.choiceRepository = choiceRepository;
        this.choiceHistoryRepository = choiceHistoryRepository;
        this.getApiOrders = getApiOrders;
    }

    @Override
    public List<Choice> getAllUsersChoices() {
        List<SqlChoice> sqlChoices = this.choiceRepository.findAll();
        return convertSqlChoicesToChoices(sqlChoices);
    }

    @Override
    public void updateChoices(List<Choice> choices) {
        try {
            for(Choice choice : choices) {
                SqlChoice selectedChoice = this.choiceRepository.findFirstByIdOrder(choice.getOrder().getId());

                if(selectedChoice == null) {
                    SqlChoice newChoice = new SqlChoice(choice.getOrder().getUserId(),choice.getOrder().getId(),choice.isRefund());
                    SqlChoiceHistory newChoiceHistory = new SqlChoiceHistory(choice.getOrder().getUserId(),choice.getOrder().getId(),choice.isRefund());

                    this.choiceHistoryRepository.save(newChoiceHistory);
                    this.choiceRepository.save(newChoice);
                } else {
                    selectedChoice.setRefund(choice.isRefund());
                    this.choiceRepository.save(selectedChoice);

                    SqlChoiceHistory choiceHistory = new SqlChoiceHistory(choice.getOrder().getUserId(),choice.getOrder().getId(),choice.isRefund());
                    this.choiceHistoryRepository.save(choiceHistory);
                }
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<Choice> getUserChoicesHistory(Integer idUser) {
        List<SqlChoiceHistory> sqlChoices = choiceHistoryRepository.findAllByIdUser(idUser);

        return convertSqlChoicesHistoryToChoices(sqlChoices);
    }

    @Override
    public List<Choice> getUserLastChoice(Integer idUser) {
        List<SqlChoice> sqlChoices = choiceRepository.findAllByIdUser(idUser);

        return convertSqlChoicesToChoices(sqlChoices);
    }

    @Override
    public void deleteAll() {
        this.choiceRepository.deleteAll();
    }

    private List<Choice> convertSqlChoicesToChoices(List<SqlChoice> sqlChoices) {
        List<Choice> choices = new ArrayList<Choice>();

        for(SqlChoice choice : sqlChoices) {
            List<Order> orders = getApiOrders.execute(choice.getIdUser());
            Order order = orders.stream().
                    filter(p -> p.getId().equals(choice.getIdOrder())).
                    findAny().orElse(null);

            choices.add(new Choice(choice.isRefund(),order));
        }

        return choices;
    }

    private List<Choice> convertSqlChoicesHistoryToChoices(List<SqlChoiceHistory> sqlChoices) {
        List<Choice> choices = new ArrayList<Choice>();

        for(SqlChoiceHistory choice : sqlChoices) {
            List<Order> orders = getApiOrders.execute(choice.getIdUser());
            Order order = orders.stream().
                    filter(p -> p.getId().equals(choice.getIdOrder())).
                    findAny().orElse(null);

            choices.add(new Choice(choice.isRefund(),order));
        }

        return choices;
    }
}
