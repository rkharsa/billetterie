package com.esgi.ecommerce.boxoffice.choice.use_case;

import com.esgi.ecommerce.boxoffice.choice.domain.ChoiceDAO;
import org.springframework.stereotype.Service;

@Service
public class DeleteChoicesImpl implements DeleteChoices {

    private final ChoiceDAO choiceDAO;

    public DeleteChoicesImpl(ChoiceDAO choiceDAO) {
        this.choiceDAO = choiceDAO;
    }

    @Override
    public void execute() {
        this.choiceDAO.deleteAll();
    }
}
