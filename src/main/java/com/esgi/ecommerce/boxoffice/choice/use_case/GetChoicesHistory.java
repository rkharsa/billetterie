package com.esgi.ecommerce.boxoffice.choice.use_case;

import com.esgi.ecommerce.boxoffice.choice.domain.Choice;

import java.util.List;

public interface GetChoicesHistory {
    List<Choice> execute(Integer idUser);
}
