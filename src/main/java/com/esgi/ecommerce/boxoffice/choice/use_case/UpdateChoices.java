package com.esgi.ecommerce.boxoffice.choice.use_case;

import com.esgi.ecommerce.boxoffice.choice.domain.Choice;
import com.esgi.ecommerce.boxoffice.order.domain.Order;

import java.util.List;

public interface UpdateChoices {
    void execute(List<Order> orders, List<Choice> choices, String email);
}
