package com.esgi.ecommerce.boxoffice.order.domain;

import java.util.List;

public interface OrderDAO {
    List<Order> getApiOrders(Integer userId);
}
