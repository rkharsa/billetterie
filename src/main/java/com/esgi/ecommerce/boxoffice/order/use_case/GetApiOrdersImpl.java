package com.esgi.ecommerce.boxoffice.order.use_case;

import com.esgi.ecommerce.boxoffice.order.domain.Order;
import com.esgi.ecommerce.boxoffice.order.domain.OrderDAO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetApiOrdersImpl implements GetApiOrders {

    private final OrderDAO orderDAO;

    public GetApiOrdersImpl(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Override
    public List<Order> execute(Integer userId) {
        List<Order> orders = orderDAO.getApiOrders(userId);
        return orders;
    }
}
