package com.esgi.ecommerce.boxoffice.order.use_case;

import com.esgi.ecommerce.boxoffice.order.domain.Order;

import java.util.List;

public interface GetApiOrders {
    List<Order> execute(Integer userId);
}
