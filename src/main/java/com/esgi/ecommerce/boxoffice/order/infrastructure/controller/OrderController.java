package com.esgi.ecommerce.boxoffice.order.infrastructure.controller;

import com.esgi.ecommerce.boxoffice.order.domain.Order;
import com.esgi.ecommerce.boxoffice.order.use_case.GetApiOrders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    private final GetApiOrders getApiOrders;

    @Autowired
    public OrderController(GetApiOrders getApiOrders){
        this.getApiOrders = getApiOrders;
    }

    @GetMapping("/{idUser}")
    private ResponseEntity getListOrderByIdUser(@PathVariable Integer idUser){
        try {
            List<Order> orders = this.getApiOrders.execute(idUser);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(orders);
        } catch(HttpClientErrorException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to get orders");
        }
    }
}
