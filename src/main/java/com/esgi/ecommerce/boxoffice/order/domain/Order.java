package com.esgi.ecommerce.boxoffice.order.domain;

public class Order {

    private Integer id;

    private float price;

    private Integer userId;

    public Order(Integer id, float price, Integer userId) {
        this.id = id;
        this.price = price;
        this.userId = userId;
    }

    public Order() {}

    public Integer getUserId() {
        return userId;
    }

    public float getPrice() {
        return price;
    }

    public Integer getId() {
        return id;
    }
}
