package com.esgi.ecommerce.boxoffice.order.infrastructure.dao;

import com.esgi.ecommerce.boxoffice.order.domain.Order;
import com.esgi.ecommerce.boxoffice.order.domain.OrderDAO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ApiOrderDAO implements OrderDAO {

    private final RestTemplate restTemplate;

    public ApiOrderDAO(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Order> getApiOrders(Integer userId) {
        try {
            ResponseEntity<List<Order>> response = restTemplate.exchange(
                    "http://demo2009247.mockable.io/user/" + userId + "/order",
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });
            return response.getBody();
        } catch (final HttpClientErrorException e) {
            return null;
        }
    }
}
