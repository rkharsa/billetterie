package com.esgi.ecommerce.boxoffice.user.infrastructure.controller;

import com.esgi.ecommerce.boxoffice.user.domain.User;
import com.esgi.ecommerce.boxoffice.user.use_case.GetApiUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/user")
public class UserController {

    private final GetApiUser getApiUser;

    @Autowired
    public UserController(GetApiUser getApiUser){
        this.getApiUser = getApiUser;
    }

    @GetMapping("/{id}")
    private ResponseEntity getUser(@PathVariable Integer id) {
        try {
            User user = this.getApiUser.execute(id);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(user);
        } catch(HttpClientErrorException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to get user");
        }
    }

}
