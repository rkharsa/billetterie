package com.esgi.ecommerce.boxoffice.user.infrastructure.dao;

import com.esgi.ecommerce.boxoffice.user.domain.User;
import com.esgi.ecommerce.boxoffice.user.domain.UserDAO;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiUserDAO implements UserDAO {

    private final RestTemplate restTemplate;

    public ApiUserDAO(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public User getApiUser(Integer userId) {
        try {
            return restTemplate.getForObject("http://demo2009247.mockable.io/user/"+userId, User.class);
        } catch (final HttpClientErrorException e) {
            return  null;
        }
    }
}
