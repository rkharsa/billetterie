package com.esgi.ecommerce.boxoffice.user.use_case;

import com.esgi.ecommerce.boxoffice.user.domain.User;

public interface GetApiUser {
    User execute(Integer userId);
}
