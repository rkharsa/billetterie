package com.esgi.ecommerce.boxoffice.user.use_case;

import com.esgi.ecommerce.boxoffice.user.domain.User;
import com.esgi.ecommerce.boxoffice.user.domain.UserDAO;
import org.springframework.stereotype.Service;

@Service
public class GetApiUserImpl implements GetApiUser {

    private final UserDAO userDAO;

    public GetApiUserImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public User execute(Integer userId) {
        return userDAO.getApiUser(userId);
    }
}
