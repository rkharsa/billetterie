package com.esgi.ecommerce.boxoffice.user.domain;

public interface UserDAO {
    User getApiUser(Integer userId);
}
