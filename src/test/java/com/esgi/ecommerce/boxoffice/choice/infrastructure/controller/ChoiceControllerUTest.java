package com.esgi.ecommerce.boxoffice.choice.infrastructure.controller;

import com.esgi.ecommerce.boxoffice.choice.domain.ConfirmationEmail;
import com.esgi.ecommerce.boxoffice.choice.infrastructure.dao.SqlChoiceDAO;
import com.esgi.ecommerce.boxoffice.choice.use_case.*;
import com.esgi.ecommerce.boxoffice.order.use_case.GetApiOrders;
import com.esgi.ecommerce.boxoffice.user.use_case.GetApiUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ChoiceControllerUTest {
    @Autowired
    private ChoiceController choiceController;

    @Autowired
    private UpdateChoices updateChoices;
    @Autowired
    private GetChoices getChoices;
    @Autowired
    private GetLastChoices getLastChoices;
    @Autowired
    private GetChoicesHistory getChoicesHistory;
    @Autowired
    private DeleteChoices deleteChoices;
    @Autowired
    private GetApiOrders getApiOrders;
    @Autowired
    private GetApiUser getApiUser;
    @Autowired
    private ConfirmationEmail emailSenderDAO;

    @Mock
    private SqlChoiceDAO choiceDAO;

    @BeforeEach
    void setUp() {
        updateChoices = new UpdateChoicesImpl(choiceDAO, emailSenderDAO);
        choiceController = new ChoiceController(updateChoices, getChoices, getLastChoices, getChoicesHistory, deleteChoices, getApiOrders, getApiUser);
    }


    @Nested
    class UpdateChoicesShould {

        @Test
        void return_ok_when_updating_choices() {
            // given
            final Integer userId = 456;

            final ChoiceDto choiceDto1 = new ChoiceDto(7,true);
            final ChoiceDto choiceDto2 = new ChoiceDto(8,false);
            List<ChoiceDto> choiceDtoList = Arrays.asList(choiceDto1, choiceDto2);
            final ChoiceMakerDto choiceMakerDto = new ChoiceMakerDto(userId, choiceDtoList);

            // when
            final ResponseEntity result = choiceController.addOrUpdateChoices(choiceMakerDto);

            // then
            assertEquals(result.getBody(),"Your choices have been successfully made");
            assertEquals(result.getStatusCode(),HttpStatus.CREATED);
        }

        @Test
        void return_bad_request_when_not_making_choice_for_all_orders() {
            // given
            final Integer userId = 456;

            final ChoiceDto choiceDto1 = new ChoiceDto(7,true);
            List<ChoiceDto> choiceDtoList = Arrays.asList(choiceDto1);
            final ChoiceMakerDto choiceMakerDto = new ChoiceMakerDto(userId, choiceDtoList);

            // when
            final ResponseEntity result = choiceController.addOrUpdateChoices(choiceMakerDto);

            // then
            assertEquals(result.getBody(),"You must make a choice for all your orders");
            assertEquals(result.getStatusCode(),HttpStatus.BAD_REQUEST);
        }

        @Test
        void return_bad_request_when_making_choices_for_unauthorized_orders() {
            // given
            final Integer userId = 456;

            final ChoiceDto choiceDto1 = new ChoiceDto(7,true);
            final ChoiceDto choiceDto2 = new ChoiceDto(5,false); // doesn't own the order
            List<ChoiceDto> choiceDtoList = Arrays.asList(choiceDto1, choiceDto2);
            final ChoiceMakerDto choiceMakerDto = new ChoiceMakerDto(userId, choiceDtoList);

            // when
            final ResponseEntity result = choiceController.addOrUpdateChoices(choiceMakerDto);

            // then
            assertEquals(result.getBody(),"You can only make choices for your own orders");
            assertEquals(result.getStatusCode(),HttpStatus.BAD_REQUEST);
        }


    }
}
